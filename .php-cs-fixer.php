<?php

$config = new PhpCsFixer\Config();
return $config
    ->setRiskyAllowed(true)
    ->setRules([
        '@PSR12'            => true,
        'array_indentation' => true,
        'array_syntax'      => [
            'syntax' => 'short',
        ],
        'combine_consecutive_unsets'  => true,
        'method_chaining_indentation' => true,
        'class_attributes_separation' => [
            'elements' => [
                'const'        => 'none',
                'method'       => 'one',
                'property'     => 'none',
                'trait_import' => 'none',
            ],
        ],
        'multiline_whitespace_before_semicolons' => [
            'strategy' => 'no_multi_line',
        ],
        'single_quote' => false,

        'binary_operator_spaces' => [
            'default'   => 'single_space',
            'operators' => [
                '='  => 'align_single_space_minimal',
                '=>' => 'align_single_space_minimal',
            ],
        ],
        'braces' => [
            'allow_single_line_closure' => true,
        ],
        'concat_space' => [
            'spacing' => 'one',
        ],
        'declare_equal_normalize'   => true,
        'function_typehint_space'   => true,
        'single_line_comment_style' => [
            'comment_types' => [
                'hash',
            ],
        ],
        'include'              => true,
        'lowercase_cast'       => true,
        'no_extra_blank_lines' => [
            'tokens' => [
                'curly_brace_block',
                'extra',
                'parenthesis_brace_block',
                'throw',
            ]
        ],
        'no_multiline_whitespace_around_double_arrow' => true,
        'no_spaces_around_offset'                     => true,
        'no_unused_imports'                           => true,
        'no_whitespace_before_comma_in_array'         => true,
        'no_whitespace_in_blank_line'                 => true,
        'object_operator_without_whitespace'          => true,
        'single_blank_line_before_namespace'          => true,
        'ternary_operator_spaces'                     => true,
        'trim_array_spaces'                           => true,
        'unary_operator_spaces'                       => true,
        'whitespace_after_comma_in_array'             => true,
        'single_line_after_imports'                   => true,
        'ordered_imports'                             => [
            'sort_algorithm' => 'none',
        ],
        //Other rules here...
    ])
    ->setLineEnding("\n");
