<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function showLogin(Request $request)
    {
        //$token = $request->session()->token();

        //$token = csrf_token();
        return view('back.login', ["title" => "The Den"]);
    }

    public function memberAuth(Request $request): Response
    {
        $token = csrf_token();

        $credentials = $request->validate([
            'handle'   => ['required'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('den');
        }

        return back()->withErrors([
            'error' => 'Nope. Check your crendtials, champ',
        ]);
    }

    public function leave(Request $request): Response
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->intended('login');
    }
}
