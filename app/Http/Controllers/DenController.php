<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DenController extends Controller
{
    //
    public function start(Request $request)
    {
        $member = Auth::user();
        return view('back.start', [
            'handle' => $member->handle,
            'title'  => "This is The Den"
        ]);
    }

    public function member(Request $request)
    {
        $member = Auth::user();
        return view('back.member', [
            'handle' => $member->handle,
            'title'  => "Manage Members"]);
    }

    public function location(Request $request, string $action = "index")
    {
        $member = Auth::user();
        return view('back.locations', [
            'handle' => $member->handle,
            'title'  => "Manage Locations",
            "action" => $action]);
    }
}
