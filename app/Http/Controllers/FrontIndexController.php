<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Location;
use App\Models\Source;

class FrontIndexController extends Controller
{
    private $limit = 15;

    public function start()
    {
        $locations = Location::where("active", true)->get();
        $count     = count($locations);
        $recent    = Location::where("active", true)
            ->where('block_count', '>', 2)
            ->limit(10)->orderByDesc('updated_at')->get();

        //$result = DB::select("SELECT * FROM searchlocations('$terms')");

        return view('front.index', [
            'count'  => $count,
            'recent' => $recent,
            'title'  => "The Bad Space"
        ]);
    }

    public function indexSearch(Request $request)
    {
        $terms     = $request->index_search;
        $rawSearch = $terms;
        $terms     = str_replace(",", "", $terms);
        $terms     = str_replace(" ", "|", $terms);
        $raw       = DB::select("SELECT * FROM searchlocations('$terms')");
        $results   = [];

        foreach ($raw as $item) {
            if ($item->block_count > 2) {
                array_push($results, $item);
            }
        }

        $locations = Location::where("active", true)->get();
        $count     = count($locations);
        $recent    = Location::where("active", true)
            ->where('block_count', '>', 2)
            ->limit(10)->orderByDesc('updated_at')->get();

        return view('front.index', [
            'count'   => $count,
            'recent'  => $recent,
            'title'   => "The Bad Space",
            'results' => $results
        ]);
    }

    public function about()
    {
        $sources = Source::where("active", true)->get();
        return view('front.about', [
            'title'   => "ABOUT",
            'sources' => $sources
        ]);
    }

    public function location(string $uuid = "1")
    {
        $location = Location::where("uuid", $uuid)->first();
        $name     = "NO LOCATION FOUND";
        if ($location) {
            $name = $location->name;
        }
        return view('front.location', [
            'title'    => str_replace(".", " ", $name),
            'location' => $location,
            'images'   => json_decode($location->images),
            'updated'  => $location->updated_at->format('Y M d'),
        ]);
    }

    public function listings(int $pageNum = 1)
    {
        $range     = $pageNum * $this->limit - $this->limit;
        $active    = Location::where("active", true)->where('block_count', '>', 2)->get();
        $locations = Location::where("active", true)->where('block_count', '>', 2)
            ->limit($this->limit)->offset($range)->orderByDesc('id')->get();
        $pageCount = ceil(count($active) / $this->limit);

        $next = $pageNum + 1;
        if ($next > $pageCount) {
            $next = 1;
        }

        $prev = $pageNum - 1;

        if ($prev <= 0) {
            $prev = $pageCount;
        }

        return view('front.listing', [
            'title'      => "Listings",
            "totalPages" => $pageCount,
            "prev"       => $prev,
            "next"       => $next,
            'pageNum'    => $pageNum,
            'locations'  => $locations
        ]);
    }
}
