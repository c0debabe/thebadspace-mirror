<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Location;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;
use App\Models\Source;

class LocationController extends Controller
{
    public function addLocation(Request $request)
    {
        $fields = $request->validate([
            'name'        => ['required'],
            'url'         => ['required'],
            'description' => ['required'],
            'tags'        => ['required'],
        ]);

        if ($fields) {
            $examples = [];
            $files    = $request->files->get("loc_examples");
            if ($request->hasfile('loc_examples')) {
                foreach ($request->file('loc_examples') as $file) {
                    $path = $file->store('reference');
                    array_push($examples, ["path" => $path]);
                }
            }
            $request->merge(['active' => true]);
            $request->merge(['uuid' => Uuid::uuid4()]);
            $request->merge(['images' => json_encode($examples)]);
            $request->merge(['added_by' => Auth::user()->id]);
            //NOTE: Laravel gets funky if sequencing isn't explicitly set
            $new = Location::create($request->all());
            if ($new) {
                return back()->with('message', 'New Location Added. Take a break!');
            } else {
                return back()->withErrors([
                    'error' => 'Uh oh. There was an inssue',
                ]);
            }
        } else {
            return back()->withErrors([
                'error' => 'All fields are required',
            ]);
        }
    }

    public function updateLocations()
    {
        $duplicates = 0;
        $fresh      = 0;

        $unified = [];
        $sources = Source::where("active", true)->get();
        foreach ($sources as $source) {
            //parsing for mastodon
            if ($source->type == 'mastodon') {
                $result = [];
                if ($source->token == null) {
                    $result = \Mastodon::domain('https://' . $source->url)
                        ->get('/instance/domain_blocks');
                } else {
                    $result = \Mastodon::domain('https://' . $source->url)
                        ->token($source->token)
                        ->get('/instance/domain_blocks');
                }

                foreach ($result as $item) {
                    $index = array_search($item['domain'], array_column($unified, 'url'));
                    if ($index) {
                        //if there is a match, update the count
                        ++$unified[$index]['count'];
                    } else {
                        array_push($unified, [
                            'name'    => $item['domain'],
                            'url'     => $item['domain'],
                            'rating'  => $item['severity'],
                            'comment' => $item['comment'],
                            'count'   => 1,
                        ]);
                    }
                }
            }
            //parsing for custom csv
            if ($source->type == 'custom' && $source->format == 'csv') {
                $denylist = array_map('str_getcsv', file($source->url));
                foreach ($denylist as $item) {
                    $index = array_search($item[0], array_column($unified, 'url'));
                    if ($index) {
                        //if there is a match, update the count
                        ++$unified[$index]['count'];
                    } else {
                        array_push($unified, [
                            'name'    => $item[0],
                            'url'     => $item[0],
                            'rating'  => $item[1],
                            'comment' => $item[2],
                            'count'   => 1,
                        ]);
                    }
                }
            }
        }

        //get all locations and sort which are present in unified or not
        /*
        $sorted    = [];
        $listed    = 0;
        $notlisted = 0;
        foreach (Location::all() as $location) {
            if (array_search($location->url, array_column($unified, 'url'))) {
                ++$listed;
                // locations present in unfied, so updated
                array_push($sorted, [
                    'location' => $location,
                    'listed'   => true
                ]);
            } else {
                ++$notlisted;
                //locations not present
                array_push($sorted, [
                    'location' => $location,
                    'listed'   => false
                ]);
            }
        };
        */

        //once the unified list is created, update current entries or create fresh ones

        foreach ($unified as $item) {
            $location = Location::where("url", $item['url'])->first();
            if ($location) {
                ++$duplicates;
                //update block count for existing item

                $location->block_count = $item['count'];

                //replace null with empty array
                if ($location->images == null) {
                    $location->images = [];
                };
                $location->save();
            } else {
                // make new entries for instances not present
                ++$fresh;
                $images = [];
                $rating = ($item['rating'] == 'defederate') ? 'suspend' : $item['rating'];
                $new    = Location::create([
                    'uuid'        => Uuid::uuid4(),
                    'name'        => $item['url'],
                    'url'         => $item['url'],
                    'description' => ($item['comment'] != null) ? $item['comment'] : "no description",
                    'active'      => true,
                    'rating'      => $rating,
                    'added_by'    => 1,
                    'tags'        => 'poor moderation, hate speech',
                    'images'      => json_encode($images),
                    'block_count' => $item['count'],
                ]);
            }
        }

        return back()->with('message', $duplicates . ' UPDATED - ' . $fresh . ' CREATED');
    }
}
