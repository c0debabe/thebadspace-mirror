<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'url'         => $this->url,
            'name'        => $this->name,
            'description' => $this->description,
            'rating'      => $this->rating,
            'count'       => $this->block_count,
            'link'        => "/location/" . $this->uuid,
        ];
    }
}
