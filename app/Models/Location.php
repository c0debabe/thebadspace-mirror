<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "location";

    protected $primaryKey = 'id';
    public $incrementing  = true;
    protected $fillable   = [
        "uuid",
        "name",
        "url",
        "description",
        "images",
        "active",
        "rating",
        "added_by",
        "tags",
        "block_count",
        "created_at",
        "updated_at"
    ];
}
