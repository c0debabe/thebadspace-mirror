<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\User as Authenticatable;

class Member extends Authenticatable
{
    use HasFactory;

    protected $table    = "member";
    protected $fillable = ["uuid", "handle", "email", "password", "active", "role", "avatar", "pronoun", "gender"];
}
