@extends('frame') 

@section('title', 'Den | Location Admin')

  @section('main-content')
    @parent
    <section>
      <article>
        <h2>Location Listings</h2>
        @if($action === "add")
          @include('forms.add-location') 
        @elseif($action === "edit") 
          EDIT LOCATION
        @elseif($action === "bulk-add") 
          ADD MANY LOCATIONS
        @else
          START
        @endif
      </article>
    </section>
  @endsection