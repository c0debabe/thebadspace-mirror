@extends('frame') 

@section('title', 'Login')

  @section('main-content')
    @parent
    <section>
      <article>
        <h2>Hey, Rando. </h2>
        <form action="/login" method="post" enctype="multipart/form-data">
          @csrf
          <label>Handle</label><br />
          <input type="text" name="handle" value="" />
          <br />
          <label>Password</label><br />
          <input type="password" name="password" value="" /><br />
          <input type="submit" value="Knock Knock" name="submit_button">
        </form>
      </article>
    </section>
  @endsection