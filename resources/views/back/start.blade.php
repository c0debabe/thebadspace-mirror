@extends('frame') 

@section('title', 'Den|Start')

  @section('main-content')

    <section>
      <article>
        <h2>Hey {{$handle}} </h2>
        <a href="/den/member">Manage Member</a><br />
        <a href="/den/locations">Manage Location</a>
      </article>
    </section>
  @endsection