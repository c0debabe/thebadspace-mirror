<form action="/den/locations/add" method="post" enctype="multipart/form-data">
  <div>
    <label>Name</label><br />
    <input type="text" name="name" value="" />
    <br />
    <label>URL</label><br />
    <input type="text" name="url" value="" />
    <br />
    <label>Tags</label><br />
    <input type="text" name="tags" value="" />
    <br />
    <label>Description</label><br />
    <textarea name="description"></textarea>
    <br />
    <label>Rating</label><br />
    <select name="rating">
      <option value="" disabled selected>Choose Rating
      </option>
      <option value="silence">Silence</option>
      <option value="defederate">Defederate</option>
    </select>
    <br />
    <label>Images</label><br />
    <input type="file" id="loc_examples" name="loc_examples[]" multiple />
  </div>
  @csrf
  <input type="hidden" name="mode" value="add" />
  <input type="submit" value="Edit Location" name="submit_button">
</form>