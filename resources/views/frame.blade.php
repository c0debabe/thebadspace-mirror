<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="theme-color" content="#d66365" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>
    @yield('title') 
  </title>
  <link rel="stylesheet" type="text/css" href="/assets/css/front/start.css?=sdfsdf">
</head>

<body>

  <header>
    <div>
      <div class="header-left">
        <a href="/">
          <img src="/assets/images/global/logo-dark.svg" title="bad-space-logo" />
        </a>
      </div>
      <div class="header-center">
        <h1>{{$title}}</h1>
      </div>
      <div class="header-right">
        <label for="element-toggle">
          <img class="menu-icon" src="/assets/images/global/menu.svg" title="menu-open-toggle" />
        </label>
        <input id="element-toggle" type="checkbox" />
        <div id="main-nav">
          <nav>
            <label for="element-toggle">
              <img class="menu-icon" src="/assets/images/global/close.svg" title="menu-open-toggle" />
            </label><br>
            <a href="/" title="front" class="nav-links">
              Front
            </a><br />
            <a href="/about" title="about" class="nav-links">
              About
            </a><br />
            <a href="/listings/1" title="instance listing" class="nav-links">
              Listings
            </a><br />
            @if(Auth::check())
              <a href="/den" title="den-start" class="nav-links">
                Den
              </a><br />
              <a href="/logout" title="logout" class="nav-links">
                Logout
              </a><br />
            @else
              <a href="/den" title="login" class="nav-links">
                The Den
              </a><br />
            @endif
          </nav>
        </div>
      </div>
    </div>
  </header>

  @if($errors->any())
    <div class="system-notice-error" role="status">
      {{$errors->first()}}
    </div>
  @endif
  @if(session('message'))
    <div class="system-notice-message" role="status">
      {!! session('message') !!}
    </div>
  @endif

  <main>
    @section('main-content')
      @show
  </main>
  <footer>
    <div>
      <p>The Bad Space © 2023</p>
    </div>
  </footer>
</body>
</body>

</html>