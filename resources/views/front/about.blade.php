@extends('frame') 
@section('title', 'The Bad Space|About')
  @section('main-content')
    @parent
    <section>
      <article>
        <h2 id="what">What is The Bad Space?</h2>
        <p>The Bad Space project was born from a need to effectively identify instances that house bad actors and are poorly moderated, which puts marginalized communities at risk.
        </p>
        <p>
          It is an extension of the
          <strong>#fediblock</strong>
          hashtag created by
          <a href="https://www.artistmarciax.com/">Artist Marcia X</a>
          with additional support from
          <a href="https://digital.rooting.garden">Ginger</a>
          to provide a catalog of instances that seek to cause harm and reduce the quality of experience in the fediverse.
        </p>
        <p>
          Technical support provided by
          <a href="https://roiskinda.cool/profile.html">Ro</a>.
        </p>
        <h2 id="how">How does it work?</h2>
        <p>The Bad Space is a collaboration of instances committed to actively moderating against racism, sexism, heterosexism, transphobia, ableism, casteism, or religion.</p>

        <p>These instances have permitted The Bad Space to read their respective blocklists to create a composite directory of sites tagged for the behavior above that can be searched and, through a public API, can be integrated into external services.</p>

        <p><strong>Current Sources:</strong></p>
        Maston:<br />
        @foreach($sources as $source)
          @if($source->format == 'json')
            <a href="https://{{$source->url}}">{{$source->url}}</a><br />
          @endif
        @endforeach
        Custom CSV<br />
        @foreach($sources as $source)
          @if($source->format == 'csv')
            <a href="{{$source->url}}">{{$source->url}}</a><br />
          @endif
        @endforeach

        <h2>How do I use it?</h2>
        <p>
          The Bad Space is meant to be a resource for anyone looking to improve the quality of their online experience by creating a tool that catalogs sources for harassment and abuse. There are several options for how it can be used.
        <h3>Search</h3>
        To see if a site is listed in the database, use the
        <a href="/">search feature</a>
        to search for that URL. If it is in the database, information for that instance will be returned and associated instances if applicable.
        <h3>CSV Exports</h3>
        For a list of the current locations being tracked, click on one of the links below to download a dynamically generated CSV file that can be consumed as a blocklist. More formats will be added over time.
        <br />
        <a href="/exports/mastodon">For Mastodon</a>
        <h3>API</h3>
        The Bad Space has a public api that can be used to search the database programatically and return results in the JSON format. The API can be accsess at<br />
        <code>https://thebad.space/api/v1/search</code>
        by posting a JSON object with the following format:
        <code>{"url":"search.url"}</code><br />
        Data from API request will be returned in the follow format:<br />

        <pre>
        <code>{
            data:{
                "listingCount":1,
                  "locations":
                  [
                    {
                      "url":"search.url",
                      "name":"Instance Name",
                      "description":"instance description",
                      "link":"bad-space-instance-link"
                    }
                  ]
            }
        }</code>
        </pre>
        </p>
      </article>
    </section>
  @endsection