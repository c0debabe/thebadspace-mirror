@extends('frame') 
@section('title', 'This is The Bad Space')
  @section('main-content')
    @parent
    <section class="index-search">
      <form class="index-search-form" action="/search" method="post" enctype="multipart/form-data">
        <input type="text" name="index_search" value="" placeholder="Hi! This is where you search." />
        <button aria-label="search-button">
          <img class="button-icon" src="assets/images/global/icon-search.svg" />
        </button>
        @csrf
      </form>
    </section>
    @isset($results)
      <section>
        <article>
          <strong>
            Found {{count($results)}} results:
          </strong><br>
          @foreach($results as $location)
            <a role="listitem" href="/location/{{$location->uuid}}">{{$location->name}}</a></a><br />
          @endforeach
        </article>
      </section>
    @endisset
    <section class="index-meta">
      <article>
        <h2>Bad Space Stats</h2>
        <strong>{{$count}}</strong><br />
        Instances being tracked.
        <h2>Recent Updates</h2>
        @foreach($recent as $location)
          <a class="list-link" role="listitem" href="/location/{{$location->uuid}}">
            <span>{{$location->block_count}}</span>
            @if($location->rating == 'silence')
              <img class="menu-icon" src="/assets/images/global/status-silence.svg" title="silenced" />
            @else
              <img class="menu-icon" src="/assets/images/global/status-suspend.svg" title="suspended" />
            @endif
            <label>{{$location->name}}</label>
          </a>
        @endforeach
      </article>
    </section>
  @endsection