@extends('frame') 
@section('title', 'The Bad Space|Listings')
  @section('main-content')
    @parent
    <section>
      <article role="list">
        <h2>Page {{$pageNum}}</h2>
        <a href="/listings/{{$prev}}">PREV</a>
        {{$pageNum}} of {{$totalPages}}
        <a href="/listings/{{$next}}">NEXT</a><br /><br />
        @foreach($locations as $location)
          <a class="list-link" role="listitem" href="/location/{{$location->uuid}}">
            @if($location->rating == 'silence')
              <span>{{$location->block_count}}</span>
              <img class="menu-icon" src="/assets/images/global/status-silence.svg" title="silenced" />
            @else
              <span>{{$location->block_count}}</span>
              <img class="menu-icon" src="/assets/images/global/status-suspend.svg" title="suspended" />
            @endif
            <label>{{$location->name}}</label>
          </a>
        @endforeach
        <br />
        <a href="/listings/{{$prev}}">PREV</a>
        {{$pageNum}} of {{$totalPages}}
        <a href="/listings/{{$next}}">NEXT</a>
      </article>
    </section>
  @endsection