@extends('frame') 

@section('title', 'The Bad Space | Location Info')

  @section('main-content')
    @parent
    <section>
      <article>
        <h1 class="location-title">{{$title}}</h1>
        <h2>Description</h2>
        {{$location->description}}<br />
        <h2>Screens</h2>
        @if($images != null)
          @foreach($images as $image)
            <a href="/{{$image->path}}" class="location-image" style="background: url(/{{$image->path}}) no-repeat center center / cover #fc6399" />
            </a>
          @endforeach
        @endif
        <br />
        @if($location->rating == 'silence')
          <img class="rating-icon" src="/assets/images/global/status-silence.svg" title="silenced" />
          <strong>Silenced Count: {{$location->block_count}}</strong>
        @else
          <img class="rating-icon" src="/assets/images/global/status-suspend.svg" title="suspended" />
          <strong>Suspended Count: {{$location->block_count}}</strong>

        @endif
        <br />
        This count reflects the number of times this instance has been suspended or silenced be two or more <a href="/about#how">Current Sources</a>

        <br />UPDATED : {{$updated}}
      </article>
    </section>
  @endsection