<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\LocationCollection;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// public search API
Route::post("/v1/search", function (Request $request) {
    $data    = json_decode($request->getContent());
    $search  = $data->url;
    $search  = str_replace(",", "", $search);
    $search  = str_replace(" ", "|", $search);
    $results = DB::select("SELECT * FROM searchlocations('$search')");
    return new LocationCollection($results);
});
