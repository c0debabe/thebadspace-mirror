<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontIndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DenController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\ExportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//front
Route::get("/", [FrontIndexController::class, 'start']);
Route::get("/listings/{pageNum}", [FrontIndexController::class, 'listings']);
Route::get("/about", [FrontIndexController::class, 'about']);
Route::get("/location/{uuid}", [FrontIndexController::class, 'location']);
Route::post("/search", [FrontIndexController::class, 'indexSearch']);

//exports
Route::get("/exports/test", [ExportController::class, 'exportCSV']);

//auth
Route::get("/login", [AuthController::class, 'showLogin']);
Route::post("/login", [AuthController::class, 'memberAuth']);
Route::get("/logout", [AuthController::class, 'leave']);

//back
Route::group(['prefix' => 'den', 'middleware' => 'member.check'], function () {
    Route::get("/", [DenController::class, 'start']);
    Route::get("/member", [DenController::class, 'member']);
    Route::get("/locations/{action?}", [DenController::class, 'location']);
    Route::post("/locations/add", [LocationController::class, 'addLocation']);
    //admin actions
    Route::get("/admin/update", [LocationController::class, 'updateLocations']);
});
